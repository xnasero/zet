# zet commands - cheatsheet

* create zet: `zet create "name of title"`
* list all titles: `zet edit|titles`
* search: `zet edit|find [searchterm]`
* search tags: `zet edit|find [#tagname]`
* get markdown formatted links: `zet link [searchterm]` (useful within vim with !!)
* get Github search link: `zet query [searchterm]`
* get last zettel: `zet last`
* get local path to zettel: `zet get [searchterm]`
