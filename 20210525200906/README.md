# Basic IRC commands

IRC servers:
• irc.freenode.net
• irc.spotchat.org

change nickname: /nick foo
close window: /close
like in discord @nickname to mark person in chat: nickname: (tab-autocomplete works)
join channel: /join
whisper private msg: /query nickname
help: /help
exit: /exit
