# Anki Add-ons


## Essentials

* ImageResizer // set height: min. 500
* Customize Keyboard Shortcuts // ??
* Mini Format Pack
* Advanced Browser
* Syntax Highlighting (Enhanced Fork) // need to config: click on "use CSS", leave style at default! (read docu if unclear)

## Optional

* (Refresh Media References) interferes with i.resizer
