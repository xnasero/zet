# Sync a repo from Github to Gitlab

1. click the "+" button and create a new project and choose  run CI/CD
1. requires a Github personal access token which you can create [here]. No 
scope needed.
1. Select the repos you wanna sync

[here]: <https://github.com/settings/tokens>

Tags:
    
    #git #sync #backup #repo
